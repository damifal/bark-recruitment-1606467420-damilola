//const apiHost = 'http://localhost:4000'; // used for docker
const apiHost = 'http://bark-recruit-api.local'; // used for vagrant

// onload
$(document).ready(function(){

    $('.js-autocomplete-services').autoComplete({
        resolverSettings: {
            url: `${apiHost}/api/services`
        }
    });

    $('.js-autocomplete-services').on('autocomplete.select', function (evt, item) {
        $('#service_id').val(item.value);
    });
    $('.js-autocomplete-services').on('change', function () {
        $('#service_id').val("");
    });

    $('.js-autocomplete-location').autoComplete({
        resolverSettings: {
            url: `${apiHost}/api/locations`
        }
    });

    $('.js-autocomplete-location').on('autocomplete.select', function (evt, item) {
        $('#location_id').val(item.value);
    });
    $('.js-autocomplete-location').on('change', function () {
        $('#location_id').val("");
    });

    (new LeadsList).init();
});

class LeadsList {

    init() {
        this.loadList();
        this.initDomItems();
        this.templateListItem = Handlebars.compile($('#handlebars-template-list-item').html());
        this.templateListDetail = Handlebars.compile($('#handlebars-template-list-detail').html());
        this.initListeners();
    }

    initDomItems() {
        this.leadListDom = $('#js-lead-list');
        this.leadDetailDom = $('#js-lead-details');
        this.leadDetailContainerDom = $('#js-lead-details-container');
        this.filterServiceDom = $('input[name=service_id]');
        this.filterLocationDom = $('input[name=location_id]');
        this.searchButtonDom = $('#js-search-button');
        this.formErrorContainer = $('#form_error');
        this.noResultsContainer = $('#no_results');
        this.showMoreButton = $('#more_button');
        this.showMoreButtonLoader = $('#more_button_loading');
        this.nextPage = "";
    }

    initListeners() {
        this.leadListDom.on(
            'click', '.js-list-item',
            (event) => this.loadLeadDetail($(event.currentTarget).data('leadId'))
        );
        this.searchButtonDom.click((event) => {
            event.preventDefault();
            this.processForm();
        });
        this.showMoreButton.click((event) => {
            event.preventDefault();
            this.loadNextPage();
        });
    }

    loadNextPage() {
        if (this.nextPage) {
            this.showMoreButtonLoader.removeClass("d-none");
            this.getNextPageFromApi()
            .then(response => response.json())
                .then(lead => this.renderList(lead, true));
        }
    }

    getNextPageFromApi() {
        return fetch(
            this.nextPage,
            {
                method: 'GET'
            }
        )
    }

    processForm() {
        let valid = true;
        let serviceIdFilter = parseInt(this.filterServiceDom.val());
        let locationIdFilter = parseInt(this.filterLocationDom.val());

        if (!serviceIdFilter && !locationIdFilter) {
            valid = false;
        }
        if (valid) {
            this.formErrorContainer.addClass('d-none');
            this.loadList();
        } else {
            this.formErrorContainer.removeClass('d-none');
        }

    }

    loadLeadDetail(id) {
        this.getLeadFromApi(id)
            .then(response => response.json())
            .then(lead => this.renderLead(lead, false));
    }

    loadList() {
        this.getListFromApi()
            .then(response => response.json())
            .then(leadList => this.renderList(leadList, false));
    }

    renderLead(lead) {
        this.leadDetailContainerDom.removeClass('d-none');
        this.leadDetailDom.html(this.templateListDetail(lead));
    }

    renderList(leadList, append) {
        if (!append){
            this.leadListDom.html('');
        }

        this.showMoreButtonLoader.addClass("d-none");
        if (leadList.data && leadList.data.length > 0) {
            this.noResultsContainer.addClass('d-none');

            for (let lead of leadList.data) {
                this.leadListDom.append(this.templateListItem(lead));
            }

            if (leadList.next_page_url) {
                this.showMoreButton.removeClass('d-none');
                this.nextPage = leadList.next_page_url;
                this.showMoreButton.attr('scroll-triggered', 'no');
            } else {
                this.showMoreButton.addClass('d-none');
                this.nextPage = "";
            }
        } else {
            this.noResultsContainer.removeClass('d-none');
            this.showMoreButton.addClass('d-none');
        }
    }

    displayLead(lead) {
        let serviceIdFilter = parseInt(this.filterServiceDom.val());
        let locationIdFilter = parseInt(this.filterLocationDom.val());

        if (serviceIdFilter && lead.service_id !== serviceIdFilter) {
            return false;
        }
        if (locationIdFilter && lead.location_id !== locationIdFilter) {
            return false;
        }

        return true;
    }

    getListFromApi() {
        let baseLeadUrl = `${apiHost}/api/leads?page=1`;

        //if not initial loading
        if (this.filterServiceDom) {
            let serviceIdFilter = parseInt(this.filterServiceDom.val());
            let locationIdFilter = parseInt(this.filterLocationDom.val());
            if (serviceIdFilter) {
                baseLeadUrl = baseLeadUrl + '&serviceId=' + serviceIdFilter;
            }
            if (locationIdFilter) {
                baseLeadUrl = baseLeadUrl + '&locationId=' + locationIdFilter;
            }
        }

        return fetch(
            baseLeadUrl,
            {
                method: 'GET'
            }
        )
    }

    getLeadFromApi(id) {
        return fetch(
            `${apiHost}/api/leads/${id}`,
            {
                method: 'GET'
            }
        )
    }

}
