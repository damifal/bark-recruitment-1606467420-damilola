//const apiHost = 'http://localhost:4000'; // used for docker
 const apiHost = 'http://bark-recruit-api.local'; // used for vagrant
$(document).ready(function(){

    $('.js-autocomplete-services').autoComplete({
        resolverSettings: {
            url: `${apiHost}/api/services`
        }
    });
    $('.js-autocomplete-services').on('autocomplete.select', function (evt, item) {
        $('#service_id').val(item.value);
    });
    $('.js-autocomplete-services').on('change', function () {
        $('#service_id').val("");
    });

    $('.js-autocomplete-location').autoComplete({
        resolverSettings: {
            url: `${apiHost}/api/locations`
        }
    });

    $('.js-autocomplete-location').on('autocomplete.select', function (evt, item) {
        $('#location_id').val(item.value);
    });
    $('.js-autocomplete-location').on('change', function () {
        $('#location_id').val("");
    });

    $('.js-submit-lead').submit(function(e) {
        e.preventDefault();
        $('#form_error').html('');
        var data = $(this).serialize();
        var that = $(this);
        $.post(
            `${apiHost}/api/leads`,
            data,
            function( data) {
                if (data.status == true) {
                    that.trigger('reset');
                    $('#service_id').val('');
                    $('#location_id').val('');
                    $('#new-lead-success').modal('show');
                    $('#form_error').addClass('d-none');
                } else {
                    $('#form_error').removeClass('d-none');
                    for (let property in data.errors) {
                        $('#form_error').append(data.errors[property] + '<br/>');
                    }
                }
            }
        );
        return false;
    });
});
