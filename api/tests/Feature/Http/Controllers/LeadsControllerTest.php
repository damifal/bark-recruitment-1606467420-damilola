<?php

namespace Tests\Feature\Http\Controllers;

use App\Http\Controllers\LeadsController;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LeadsControllerTest extends TestCase
{
    /**
     * A basic endpoint response code test.
     *
     * @return void
     */
    public function testOnSuccessfulResponseTest()
    {
        $response = $this->get('/api/leads');

        $response->assertStatus(200);
    }

    /**
     * Test that the last name is obfuscated
     *
     * @return void
     */
    public function testDataIsObfuscated()
    {
        $response = $this->get('/api/leads');
        $name = $response->json()['data'][2]['name'];
        $phone = $response->json()['data'][2]['phone'];
        $email = $response->json()['data'][2]['email'];
        //last name is obfuscated
        $this->assertCount(1, explode(" ", $name));
        $this->assertGreaterThan(4, substr_count($phone, '*'));
        $this->assertGreaterThan(4, substr_count($email, '*'));
    }

    /**
     * Test that the pagination is working as expected
     *
     * @return void
     */
    public function testPagination()
    {
        $response = $this->get('/api/leads');
        $this->assertEquals(1, $response->json()['current_page']);
        $this->assertCount(LeadsController::PER_PAGE, $response->json()['data']);
        $this->assertEquals($response->json()['path'].'?page=2', $response->json()['next_page_url']);
    }
}
