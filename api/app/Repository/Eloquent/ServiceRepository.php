<?php

namespace App\Repository\Eloquent;

use App\Models\Service;
use App\Repository\ServiceRepositoryInterface;
use Illuminate\Support\Collection;

/**
 * Class ServiceRepository
 * @package App\Repository\Eloquent
 */
class ServiceRepository extends BaseRepository implements ServiceRepositoryInterface
{
    /**
     * ServiceRepository constructor.
     *
     * @param Service $model
     */
    public function __construct(Service $model)
    {
        parent::__construct($model);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return Service::all();
    }

    /**
     * @param string $query
     * @return Collection
     */
    public function search(string $query): Collection
    {
        return Service::whereRaw("name like '$query%'")->get();
    }
}