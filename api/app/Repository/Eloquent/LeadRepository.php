<?php

namespace App\Repository\Eloquent;

use App\Models\Lead;
use App\Repository\LeadRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

/**
 * Class LeadRepository
 * @package App\Repository\Eloquent
 */
class LeadRepository extends BaseRepository implements LeadRepositoryInterface
{
    /**
     * LeadRepository constructor.
     *
     * @param Lead $model
     */
    public function __construct(Lead $model)
    {
        parent::__construct($model);
    }

    /**
     * @param Request $request
     * @param int $perPage
     * @return Paginator
     */
    public function allObfuscated(Request $request, int $perPage = 10): Paginator
    {
        $builder = $this->getInitialBuilder();
        //use builder/decorator pattern in refactor
        if ($request->has('serviceId')) {
            $builder = $builder->where('services.Id',intval($request->get('serviceId')));
        }
        if ($request->has('locationId')) {
            $builder = $builder->where('locations.Id',$request->get('locationId'));
        }

        return $this->obfuscateLeads($builder->simplePaginate($perPage));
    }

    /**
     * @param int $id
     */
    public function oneObfuscated(int $id)
    {
        return $this->obfuscateLead($this->getInitialBuilder()->where('leads.id', $id)
            ->firstOrFail());
    }

    /**
     * @return Builder
     */
    private function getInitialBuilder() : Builder
    {
        return Lead::query()
            ->leftJoin('locations', 'locations.id', '=', 'leads.location_id')
            ->leftJoin('services', 'services.id', '=', 'leads.service_id')
            ->select(
                'leads.id',
                'leads.name',
                'leads.email',
                'leads.phone',
                'leads.more_info',
                'services.name as service_name',
                'locations.name as location_name'
            );
    }

    /**
     * Obfuscate Leads
     * @param Paginator $paginator
     * @return Paginator
     */
    private function obfuscateLeads(Paginator $paginator): Paginator
    {
        $paginator->getCollection()->transform(function ($lead) {
            return $this->obfuscateLead($lead);
        });

        return $paginator;
    }

    /**
     * @param $lead
     * @return mixed
     */
    private function obfuscateLead($lead)
    {
        if (!empty($lead)) {
            $lead['email'] = $this->obfuscateEmail($lead['email']);
            $lead['phone'] = $this->obfuscatePhone($lead['phone']);
            $lead['name'] = $this->obfuscateName($lead['name']);
        }
        return $lead ;
    }

    /**
     * @param string $email
     * @return string
     */
    private function obfuscateEmail(string $email): string
    {
        return str_repeat("*", strlen($email)-4) . substr($email, -4);
    }

    /**
     * @param string $phone
     * @return string
     */
    private function obfuscatePhone(string $phone): string
    {
        return substr($phone, 0, 4).str_repeat("*", strlen($phone)-4) ;
    }

    /**
     * @param string $name
     * @return string
     */
    private function obfuscateName(string $name): string
    {
        return explode(" ", $name)[0];
    }
}