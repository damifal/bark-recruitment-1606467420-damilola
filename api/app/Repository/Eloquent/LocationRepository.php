<?php

namespace App\Repository\Eloquent;

use App\Models\Location;
use App\Repository\LocationRepositoryInterface;
use Illuminate\Support\Collection;

/**
 * Class LocationRepository
 * @package App\Repository\Eloquent
 */
class LocationRepository extends BaseRepository implements LocationRepositoryInterface
{
    /**
     * LocationRepository constructor.
     *
     * @param Location $model
     */
    public function __construct(Location $model)
    {
        parent::__construct($model);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return Location::all();
    }

    /**
     * @param string $query
     * @return Collection
     */
    public function search(string $query): Collection
    {
        return Location::whereRaw("name like '$query%'")->get();
    }
}