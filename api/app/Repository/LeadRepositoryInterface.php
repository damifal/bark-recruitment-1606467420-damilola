<?php

namespace App\Repository;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

/**
 * Interface LeadRepositoryInterface
 * @package App\Repository
 */
interface LeadRepositoryInterface
{
    public function allObfuscated(Request $request, int $perPage): Paginator;

    public function oneObfuscated(int $id);
}
