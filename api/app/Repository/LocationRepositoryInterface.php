<?php

namespace App\Repository;

use Illuminate\Support\Collection;

/**
 * Interface LocationRepositoryInterface
 * @package App\Repository
 */
interface LocationRepositoryInterface
{
    public function all(): Collection;

    public function search(string $query): Collection;
}
