<?php

namespace App\Repository;

use Illuminate\Support\Collection;

/**
 * Interface ServiceRepositoryInterface
 * @package App\Repository
 */
interface ServiceRepositoryInterface
{
    public function all(): Collection;

    public function search(string $query): Collection;
}
