<?php namespace App\Http\Controllers;


namespace App\Http\Controllers;

use App\Models\Lead;
use App\Repository\LeadRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Validator;


class LeadsController extends Controller
{
    public const PER_PAGE = 10;

    /**
     * @var LeadRepositoryInterface
     */
    private $leadRepository;

    /**
     * LeadsController constructor.
     * @param LeadRepositoryInterface $leadRepository
     */
    public function __construct(LeadRepositoryInterface $leadRepository)
    {
        $this->leadRepository = $leadRepository;
    }

    /**
     * Find the nearest city based on the provided lat / lng
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse {

        $messages = [
            'location_id.required' => 'Please select a location from the list.',
            'service_id.required' => 'Please select a service from the list.',
        ];

        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|max:200',
                'email' => 'required|max:200',
                'phone' => 'required|max:100',
                'extra' => 'max:1000',
                'location_id' => 'required|integer',
                'service_id' => 'required|integer',
            ],
            $messages
        );

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()
            ]);

        }

        $input = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'extra' => $request->get('extra'),
            'location_id' => $request->get('location_id'),
            'service_id' => $request->get('service_id'),
        ];

        $this->leadRepository->create($input);

        return response()->json([
            'status'    =>  true
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function all(Request $request) {
        return $this->leadRepository->allObfuscated($request, self::PER_PAGE);
    }

    public function one($id) {
        return $this->leadRepository->oneObfuscated($id);
        return Lead::findOrFail($id);
    }
}
