<?php namespace App\Http\Controllers;


namespace App\Http\Controllers;

use App\Models\Service;
use App\Repository\ServiceRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;


class ServicesController extends Controller
{
    /**
     * @var ServiceRepositoryInterface
     */
    private $serviceRepository;

    /**
     * ServicesController constructor.
     * @param ServiceRepositoryInterface $serviceRepository
     */
    public function __construct(ServiceRepositoryInterface $serviceRepository)
    {
        $this->serviceRepository = $serviceRepository;
    }

    /**
     * Get services with a possible query param to search
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request): JsonResponse {

        $query = $request->get('q');
        if ($query) {
            $services = $this->serviceRepository->search($query);
        } else {
            $services = $this->serviceRepository->all();
        }

        $servicesFormatted = [];
        foreach ($services as $service) {
            $servicesFormatted[] = [
                'value' => $service->id,
                'text' => $service->name,
            ];
        }

        return response()->json($servicesFormatted);
    }

}



