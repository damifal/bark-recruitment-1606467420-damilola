<?php namespace App\Http\Controllers;


namespace App\Http\Controllers;

use App\Models\Location;
use App\Repository\LocationRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;


class LocationsController extends Controller
{
    /**
     * @var LocationRepositoryInterface
     */
    private $locationRepository;

    /**
     * LocationsController constructor.
     * @param LocationRepositoryInterface $locationRepository
     */
    public function __construct(LocationRepositoryInterface $locationRepository)
    {
        $this->locationRepository = $locationRepository;
    }

    /**
     * Get locations with a possible query param to search
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request): JsonResponse {

        $query = $request->get('q');
        if ($query) {
            $locations = $this->locationRepository->search($query);
        } else {
            $locations = $this->locationRepository->all();
        }

        $locationsFormatted = [];
        foreach ($locations as $location) {
            $locationsFormatted[] = [
                'value' => $location->id,
                'text' => $location->name,
            ];
        }

        return response()->json($locationsFormatted);
    }

}



